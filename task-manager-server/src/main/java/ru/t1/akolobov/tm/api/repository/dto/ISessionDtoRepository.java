package ru.t1.akolobov.tm.api.repository.dto;

import ru.t1.akolobov.tm.dto.model.SessionDto;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDto> {
}
