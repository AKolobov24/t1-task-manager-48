package ru.t1.akolobov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IDatabaseProperty;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    final EntityManagerFactory entityManagerFactory;

    @NotNull
    private final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = factory();
    }

    @Override
    @NotNull
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseUser());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseDDLAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getSecondLvlCache());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(UserDto.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(SessionDto.class);
        source.addAnnotatedClass(Session.class);
        source.addAnnotatedClass(ProjectDto.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDto.class);
        source.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
