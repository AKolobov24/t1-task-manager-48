package ru.t1.akolobov.tm.exception.user;

public final class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
