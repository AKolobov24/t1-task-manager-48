package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.component.ISaltProvider;
import ru.t1.akolobov.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getCommandFolder();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationAuthor();

    @NotNull
    String getAuthorEmail();

}
